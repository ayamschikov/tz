<?php
	if ( !defined('BOOTSTRAP') ) { die('Access denied'); }
	//this function inserts into the table 'status_log' information about new change order status
		function fn_log_status_change_order_status($status_to, $status_from, $order_info, $force_notification, $order_statuses, $place_order)
{
			db_query('INSERT INTO ?:status_log ?e', 
			array(
			'id_order'=>$order_info['order_id'],
			'old_status'=>$status_from,
			'new_status'=>$status_to,
			'datetime'=>fn_parse_date(time()),
			'userid'=>$_SESSION['auth']['user_id'])
		);
}	
		//this function returned array rows tables "status_log"
	function fn_get_data_log_status()
	{
		$fields = array (
			'?:status_log.id_order',
			'?:status_log.old_status',
			'?:status_log.new_status',
			'?:status_log.userid',
			'?:status_log.datetime',
			'?:users.firstname',
			'?:users.lastname',
		);
		return db_get_array("SELECT ?p FROM ?:status_log LEFT JOIN ?:users ON ?:status_log.userid=?:users.user_id ORDER BY ?:status_log.datetime DESC", implode(", ", $fields));
	}
?>