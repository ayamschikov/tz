<?php

$schema['central']['orders']['items']['log_status'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'log_status.manage',
    'position' => 1000
);
return $schema;
