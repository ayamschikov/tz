<?php
use Tygh\Registry;

	if (!defined('BOOTSTRAP')) { die('Access denied'); }

	if ($mode == 'manage') 
	{
		$order_statuses = fn_get_statuses(STATUSES_ORDER, array(), false, true);
		$log_statuses = fn_get_data_log_status();
		Registry::get('view')->assign('log_statuses', $log_statuses);
		Registry::get('view')->assign('order_statuses', $order_statuses);
	}

