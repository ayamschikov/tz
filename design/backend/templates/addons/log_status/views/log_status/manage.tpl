{capture name="mainbox"}

	<form action="{""|fn_url}" method="post" name="log_status_form" class=" cm-hide-inputs" enctype="multipart/form-data">

	{if $log_statuses}
		<table class="table table-middle">
			<thead>
				<tr>
					<th>{__("log_status.orderid")}</th>
				    <th>{__("log_status.oldstatus")}</th>
   					<th>{__("log_status.newstatus")}</th>
					<th>{__("log_status.usereditor")}</th>
					<th>{__("log_status.dateedition")}</th>		
				</tr>
			</thead>
			{foreach from=$log_statuses item=lts}
				<tr class="cm-row-status-{$lts.status|lower}">
					<td><span class="nowrap row-status {$no_hide_input}">{$lts.id_order}</span></td>
					<td><span class="label btn-info o-status-{$lts.old_status|lower}">{$order_statuses[$lts.old_status].description}</span></td>   
					<td><span class="label btn-info o-status-{$lts.new_status|lower}">{$order_statuses[$lts.new_status].description}</span></td>
					<td>{if $lts.userid}<a href="{"profiles.update?user_id=`$lts.userid`"|fn_url}">{/if}{$lts.lastname} {$lts.firstname}{if $lts.userid}</a>{/if}</td>	                
					<td><span class="nowrap row-status {$no_hide_input}">{$lts.datetime|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</span></td>		  
				</tr>
			{/foreach}
		</table>
	{else}
		<p class="no-items">{__("no_data")}</p>
	{/if}  
	</form>

{/capture}
{include file="common/mainbox.tpl" title=__("log_status") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}
